<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/**
 * HELLOOOOOOO TEAM FERIT
 */

Route::get('profile/{id}', ['as' => 'showProfileSidebar', 'uses' => 'AppController@getProfilesFromJSON']);

Route::get('/', 'AppController@index')->name('index');

Route::post('/messages', 'AppController@storeMessage')->name('storeMessage');

