<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Message;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::all();
        return view('portfolio.index', ['messages' => $messages]);
    }

    public function storeMessage(Request $request)
    {
        $errorMessages = [
            'name.required' => 'Morate upisati ime',
            'content.required' => 'Morate upisati sadržaj poruke',
        ];

        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'content' => 'required|max:1500',
        ], $errorMessages);

        Message::create($request->all());

        $request->session()->flash('status', 'true');

        return redirect()->route('index');
    }

    public function getProfilesFromJSON(Request $request, $id) {
        if ($request->ajax()) {
            $path = storage_path() . "/profiles.json";
            $json = json_decode(file_get_contents($path), true);
            return view('portfolio.partials.sidePanelContent')->with('profile', $json[$id-1]);
        }
    }
}
