<div class="row">
    <div class="col-sm-offset-1 col-sm-10">
    <div class="row">
        <div class="col-sm-12">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="_mainHeading">Guestbook</h2>
            <p>
                <b>Pišite po našoj stranici!</b> Svaki komentar, kritika ili pohvala dobro je došla. <i class="fa fa-smile-o"></i>
            </p>

                <form action="{{ route('storeMessage') }}" method="post">
                    <div class="form-group">
                        <label for="name">Vaše ime</label>
                        <input type="text" class="form-control" id="name" name="name" required placeholder="upišite vaše ime...">
                    </div>
                    <div class="form-group">
                        <label for="content">Poruka</label>
                        <textarea class="form-control" rows="5" id="content" name="content" required placeholder="ovdje napišite vašu poruku..."></textarea>
                    </div>
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-default pull-right">Pošalji poruku</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-offset-1 col-sm-10">
                <div class="_messages">
                    @if($messages)
                        @foreach($messages as $message)
                            <div class="_message">
                                <h3>{{ $message->name }} <span class="pull-right _message-publishedAt">{{ $message->created_at->format('F j, Y') }}</span></h3>
                                <p>{{ $message->content }}</p>
                            </div>
                        @endforeach
                    @else
                        <p>Trenutačno nema poruka</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>