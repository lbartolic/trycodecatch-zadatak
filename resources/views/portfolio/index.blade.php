@extends('portfolio.portfolio')

@section('title', 'Portfolio')

@section('mainContent')
    @if(session()->has('status'))
        <script>
            swal("Hvala!", "Hvala na poruci! Sve poruke možete vidjeti na dnu stranice.", "success")
        </script>
    @endif
    <div id="body-faded-layer"></div>
    <div id="side-panel">
        <div class="_side-panel-close" id="side-panel-close">
            <i class="fa fa-close"></i>
        </div>
        <div class="container-fluid">
            <div class="row" id="side-panel__content">

            </div>
        </div>
    </div>


    <nav class="navbar navbar-default navbar-fixed-top _navbar" id="navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#main-nav-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">#FERIT</a>
            </div>
            <div class="collapse navbar-collapse" id="main-nav-collapse">
                <ul class="nav navbar-nav">
                    <li class="_nav-def"><a class="_anchorScroll" href="#home"><i class="fa fa-home"></i></a></li>
                    <li class="_nav-def"><a class="_anchorScroll" href="#aboutUs">O nama</a></li>
                    <li class="_nav-def"><a class="_anchorScroll" href="#tehnologije">Tehnologije</a></li>
                    <li class="_nav-def"><a class="_anchorScroll" href="#guestbook">Guestbook</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="_ivan _navOpenProfile"><a href="#"
                                                         data-profile="{{ route('showProfileSidebar', ['user' => 1]) }}">Ivan</a>
                    </li>
                    <li class="_luka _navOpenProfile"><a href="#"
                                                         data-profile="{{ route('showProfileSidebar', ['user' => 3]) }}">Luka</a>
                    </li>
                    <li class="_dominik _navOpenProfile"><a href="#"
                                                            data-profile="{{ route('showProfileSidebar', ['user' => 2]) }}">Dominik</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="jumbotron _portfolio-jumbo scrollto" id="home">
        <div class="container _portfolio-jumbo__inner _fadeInTop" id="portfolio-jumbo">
            <div class="row">
                <div class="col-sm-12">
                    <h1>#FERIT</h1>

                    <h2><span class="_tryCodeCatch">try{code}catch</span> team portfolio</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row _about-us scrollto" id="aboutUs">
            <h2 class="_mainHeading">Tko smo mi?</h2>

            <div class="col-sm-12 _indent">
                <p>
                    Bok! Mi smo <b>FERIT</b> tim i dolazimo s fakulteta <b>Elektrotehnike i računarstva u Osijeku</b>.
                    Članovi našega tima mogu se pohvaliti mnogim uspjesima, iskustvima i referencama koje uključuju rad
                    za vodeću svjetsku eCommerce tvrtku Inchoo, istkustvo finalista na jednome Hackathon eventu,
                    samostalno razvijanje i deployanje igre na Google Play storeu, demonstraturu laboratorijskih vježbi
                    na Elektrotehničkom fakultetu, rad za strane klijente i slične stvari.
                </p>

                <p>
                    Treba naglasiti kako smo iznimno <b>složna i timski orijentirana ekipa</b> koja zajedno sa svim
                    problemima nađe zajednički jezik kako bi ga uspješno mogli riješiti. Imamo dugogodišnje iskustvo u
                    izradi web aplikacija i stranica koje konstantno unaprijeđujemo i nadograđujemo kako bismo mogli
                    pratiti development trendove za kvalitetniju i bolju izradu aplikacija. <b>Osim proaktivnog
                        karaktera, svakoga od nas krasi bogat portfolio prijašnjih projekata i zanimljivih radova.</b>
                </p>

                <p>
                    Tim FERIT sastoji se od <b>3 člana</b> koji su ujedno i studenti Elektrotehničkog fakulteta u
                    Osijeku. Ivan i Luka studenti su 1. godine diplomskog, dok je Dominik student 2. godine
                    preddiplomskog studija računarstva.
                </p>
            </div>
        </div>
        <div class="row">
            <h2 class="_mainHeading scrollto" id="team">FERIT ekipa</h2>

            <div class="col-sm-6 col-md-4 _team-member__col">
                <div class="_team-member _fadeInTop" id="ivan"
                     data-profile="{{ route('showProfileSidebar', ['user' => 1]) }}">
                    <img src="img/team/ivan.jpg">

                    <div class="_team-member__frame">
                        <div class="_team-member__info _ivan">
                            <div class="_team-member__inner">
                                <h3>Ivan Bernatović</h3>

                                <p>Full stack web developer iz Osijeka. Interes za programiranje stekao sam zbog video
                                    igara još kao klinac ali ipak sam završio u web developmentu... <a href="#"
                                                                                                       class="_team-member__more">Vidi
                                        više</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 _team-member__col">
                <div class="_team-member _fadeInTop" id="luka"
                     data-profile="{{ route('showProfileSidebar', ['user' => 3]) }}">
                    <img src="img/team/luka3.jpg">

                    <div class="_team-member__frame">
                        <div class="_team-member__info _luka">
                            <div class="_team-member__inner">
                                <h3>Luka Bartolić</h3>

                                <p>Frontend/backend developer i ljubitelj poljsko-čeških piva. Posljednjih par godina
                                    radim kao freelancer, a s obzirom da jako volim putovati...<a href="#"
                                                                                                  class="_team-member__more">Vidi
                                        više</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 _team-member__col">
                <div class="_team-member _fadeInTop" id="dominik"
                     data-profile="{{ route('showProfileSidebar', ['user' => 2]) }}">
                    <img src="img/team/dominik.jpg">

                    <div class="_team-member__frame">
                        <div class="_team-member__info _dominik">
                            <div class="_team-member__inner">
                                <h3>Dominik Kotris</h3>

                                <p>Frontend developer/UX designer i basist. Noći koristi za testiranje i stvaranje novih
                                    stvari dok danju spava. Izvan web i glazbenog communityja... <a href="#"
                                                                                                    class="_team-member__more">Vidi
                                        više</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron _coding-jumbo" id="coding-jumbo">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-2 col-sm-4 col-xs-6">
                    <div class="_item _fadeInZoomIn">
                        <i class="fa fa-search _icon"></i>

                        <h3>Istraživanje</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="_item _fadeInZoomIn">
                        <i class="fa fa-lightbulb-o _icon"></i>

                        <h3>Plan</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="_item _fadeInZoomIn">
                        <i class="fa fa-pencil _icon"></i>

                        <h3>Dizajn</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <div class="_item _fadeInZoomIn">
                        <i class="fa fa-code _icon"></i>

                        <h3>Razvoj</h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <div class="_item _fadeInZoomIn">
                        <i class="fa fa-rocket _icon"></i>

                        <h3>Lansiranje</h3>
                    </div>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
    </div>
    <div class="container clearfix" id="tehnologije">
        <h2 class="_mainHeading">Tehnologije za realizaciju rješenja</h2>

        <div class="_tech-list__inner">
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/html.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/sass.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/js.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/php.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/mysql.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/laravel.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/linux.png">
            </div>
            <div class="_tech-item _fadeInZoomIn">
                <img src="img/tech/mac.png">
            </div>
        </div>
    </div>
    <div id="guestbook" class="container _guestbook">
        @include('portfolio.forma')
    </div>
@endsection

@section('js')
    <script>
        var lastScrollTop = 0;
        $(document).ready(function () {
            runAnimations();

            $(window).scroll(function (event) {
                runAnimations();

                var st = $(this).scrollTop();
                if (st > lastScrollTop && st > 70) {
                    $('#navbar').css({'margin-top': '-50px'});
                } else {
                    $('#navbar').css({'margin-top': '0'});
                }
                lastScrollTop = st;
            });
        });

        function runAnimations() {
            $('._fadeInTop').each(function () {
                if (isScrolledIntoView(this) == true) {
                    $(this).css({
                        opacity: 1,
                        top: 0
                    });
                }
            });
            $('._fadeInZoomIn').each(function () {
                if (isScrolledIntoView(this) == true) {
                    $(this).css({
                        transform: 'scale(1)',
                        opacity: 1
                    });
                }
            });
        }

        function isScrolledIntoView(elem) {
            var $elem = $(elem);
            var $window = $(window);
            var docViewTop = $window.scrollTop();
            var docViewBottom = docViewTop + $window.height();
            var elemTop = $elem.offset().top;

            return (((elemTop + $elem.height() / 2) <= docViewBottom) || (elemTop < docViewTop));
        }
    </script>
@endsection
