<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf_token" content="{{ csrf_token() }}" />
      <link rel="icon" href="">
      <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
      <title>FERIT - @yield('title')</title>

      {!! HTML::style('assets/css/animate.css') !!}
      {!! HTML::style('assets/css/bootstrap.min.css') !!}
      {!! HTML::style('plugins/font-awesome-4.5.0/css/font-awesome.min.css') !!}
      {!! HTML::style('assets/css/app.css') !!}
      {!! HTML::style('assets/css/sweetalert.css') !!}
      @yield('additionalCss')

      {!! HTML::script('assets/js/jquery-1.10.2.js') !!}
      {!! HTML::script('assets/js/jquery-ui.js') !!}
      {!! HTML::script('assets/js/jquery.bootstrap.wizard.min.js') !!}
      {!! HTML::script('assets/js/bootstrap.min.js') !!}
      {!! HTML::script('assets/js/isotope.min.js') !!}
      {!! HTML::script('assets/js/sweetalert.min.js') !!}

      {!! HTML::script('assets/js/source.js') !!}
      @yield('additionalJs')
   </head>

   <body class="">
       @yield('navbar')

       @yield('topContent')

       @yield('mainContent')
   </body>
</html>

@yield('js')
