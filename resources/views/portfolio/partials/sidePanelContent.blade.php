<div class="_side-panel__holder">
  <div class="_profile__top">
    <div class="row">
      <div class="col-xs-12">
        <div class="_side-panel__photo">
          <img src='{{ url('/') }}/img/team/{{ $profile["img"] }}'>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <h1>{{ $profile["ime"] }} {{ $profile["prezime"] }}</h1>
        <h2>{{ $profile["posao"] }}</h2>
      </div>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="row _side-panel__intro">
      <div class="col-xs-offset-1 col-xs-10">
        <p>
          {{ $profile["tekst"] }}
        </p>
      </div>
    </div>
    <h1 class="_side-panel__mainH">Znanje i iskustvo</h1>
    <div class="row _side-panel__interests">
      <div class="col-xs-12">
        @foreach($profile["iskustva"] as $iskustvo)
          <span class="label label-default _interest-label">{{ $iskustvo }}</span>
        @endforeach
      </div>
    </div>
    <h1 class="_side-panel__mainH">Linkovi <small>projekti i dodatne informacije</small></h1>
    <div class="row _side-panel__linkovi">
      <div class="col-xs-12">
        @foreach($profile["linkovi"] as $link)
          <a class="_side-panel__link" href='{{ $link["link"] }}' target="_blank"><i class="fa fa-external-link"></i>{{ $link["naziv"] }}</a>
        @endforeach
      </div>
    </div>
    <h1 class="_side-panel__mainH">Općenite informacije</h1>
    <div class="row _side-panel__about">
      <div class="col-sm-6 col-xs-12">
        <h2><i class="fa fa-home"></i>Mjesto rođenja</h2>
        <div class="_about-text">
          <span>{{ $profile["mjesto_rodenja"] }}</span>
        </div>
        <h2><i class="fa fa-birthday-cake"></i>Datum rođenja</h2>
        <div class="_about-text">
          <span>{{ $profile["datum_rodenja"] }}</span>
        </div>
        <h2><i class="fa fa-envelope"></i>Kontakt email</h2>
        <div class="_about-text">
          <span>{!! $profile["email"] !!}</span>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12">
        <h2><i class="fa fa-graduation-cap"></i>Fakultet</h2>
        <div class="_about-text">
          <span>{{ $profile["fakultet"] }}</span>
        </div>
        <h2><i class="fa fa-caret-square-o-up"></i>Razina studija</h2>
        <div class="_about-text">
          <span>{{ $profile["razina_studija"] }}</span>
        </div>
        <h2><i class="fa fa-random"></i>Smjer</h2>
        <div class="_about-text">
          <span>{{ $profile["smjer"] }}</span>
        </div>
      </div>
    </div>
    <h1 class="_side-panel__mainH">Hobiji i interesi</h1>
    <div class="row _side-panel__interests">
      <div class="col-xs-12">
        @foreach($profile["interesi"] as $interes)
          <span class="label label-default _interest-label">{{ $interes }}</span>
        @endforeach
      </div>
    </div>
  </div>
</div>