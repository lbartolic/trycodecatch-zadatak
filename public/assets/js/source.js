$(document).ready(function() {
	$("._anchorScroll").click(function(event) {
		event.preventDefault();
		$('html,body').animate( { scrollTop: $(this.hash).offset().top - 80 } , 600);
	});

	$('._team-member, ._navOpenProfile a').click(function(e) {
		e.preventDefault();
		var profile = $(this).attr('data-profile');
		showProfile(profile);
	});

	$('#body-faded-layer').click(function() {
		sidePanelClose();
	});

	$('._team-member__more').click(function(event) {
		event.preventDefault();
	});
});

var loadingProfile = false;
function showProfile(profile) {
	if (loadingProfile == false) {
		loadingProfile = true;
		sidePanelOpen();
		$.get(profile, function(r) {
			loadingProfile = false;
			setTimeout(function() { 
				$('#side-panel__content').html(r);
			}, 350);
		}, "html")
		.fail(function(data) {
			loadingProfile = false;
			sidePanelClose();
			console.log(data);
		});
	}
}

function sidePanelOpen() {
	$('body').addClass('_body-noscroll');
	$('#body-faded-layer').css({ 'display': 'block' });
	$('#side-panel').css({ transform: 'translateX(0)' });
	showLoadingSpinner('#side-panel__content');
	$('#_side-panel').focus();
	$('#side-panel-close').click(function() { 
		sidePanelClose(); 
	});
}

function sidePanelClose() {
	$('body').removeClass('_body-noscroll');
	$('#body-faded-layer').css({ 'display': 'none' });
	$('#side-panel').css({ transform: 'translateX(100%)' });
}

function showLoadingSpinner(element) {
	$(element).html('<div class="_loading-spinner"><i class="fa fa-circle-o-notch fa-spin"></i></div>')
}